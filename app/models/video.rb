class Video
  class << self
    def all

    end

    def find(id)
      video = _read(id)
      video["created_by"] = User.get(video["created_by"])
      video
    end

    def save(video)
      Rails.logger.info "video to save" + video.to_json
      _write(video["vid"], video)
    end

    def create(params)
      video = {}
      video["service"] = _detect_service(params["vlink"])
      video["vid"] = _get_vid(params["vlink"], video["service"])
      video["created_by"] = params["user_id"]
      video["create_time"] = Time.now.utc
      
      video_meta = _video_meta(video["vid"], video["service"])
      video.merge!(video_meta)
      
      ## Default karma is 0
      video["karma"] = "0"
      
      ## Bundle initially has no videos
      video["bundle_ids"] = [params["bundle_id"]]
      
      # Interest ids blank for now
      video["interest_ids"] = []
      
      Rails.logger.info "Video is" + video.to_json
      video
    end

    def delete(id)
    end

    private

    def _youtube_video_pic_url(vid)
      "http://img.youtube.com/vi/#{vid}/0.jpg"
    end

    def _get_vid(vlink, service)
      case service
      when "youtube" then vlink.split('?')[1].split('&')[0].split('=')[1]
      when "vimeo"   then URI(vlink).path.split("/")[1]
      else                vlink
      end
    end

    def _get_next_vid
      ## TODO fix this logic. If count is zero all keys will be overwritten
      REDIS.incr(Settings.app_key + ":videocount").to_s
    end

    def _video_meta(vid, service)
      case service
      when "youtube"
        video_meta = _youtube_video_meta(vid)
      when "vimeo"
        video_meta = _vimeo_video_meta(vid)
      else
        video_meta = _empty_video_meta(vid)
      end
      
    end

    def _youtube_video_meta(vid)
      url = URI.parse('http://gdata.youtube.com/feeds/api/videos/' + vid)
      req = Net::HTTP::Get.new(url.path + "?alt=json")
      req["Content-Type"] = "application/json"
      begin
        res = Net::HTTP.start(url.host, url.port) {|http|
                http.request(req)
              }
      rescue
        Rails.logger.info "[FAIL] To fetch video meta from youtube params vid = #{vid}"
        return _empty_video_meta(vid)
      end
      video_meta = ActiveSupport::JSON.decode res.body
      meta = {
               "description"     => video_meta["entry"]["media$group"]["media$description"]["$t"],
               "title"           => video_meta["entry"]["title"]["$t"],
               "views"           => video_meta["entry"]["yt$statistics"]["viewCount"],
               "duration"        => video_meta["entry"]["media$group"]["media$content"].first["duration"],
               "display_picture" => _youtube_video_pic_url(vid),
               "link"            => "http://www.youtube.com/watch?v=#{vid}"
              }
      meta
    end
    
    def _vimeo_video_meta(vid)
      begin
        resp = ActiveSupport::JSON.decode(RestClient.get("http://vimeo.com/api/v2/video/#{vid}.json")).first
      rescue
        Rails.logger.info "[FAIL] To fetch video meta from vimeo params vid = #{vid}"
        return _empty_video_meta(vid)
      end
      meta = {
               "description"     => resp["description"],
               "title"           => resp["title"],
               "views"           => resp["stats_number_of_plays"],
               "duration"        => resp["duration"],
               "display_picture" => resp["thumbnail_large"],
               "link"            => resp["url"]
              }
      meta
    end
    
    def _empty_video_meta(vid)
      meta = {
               "description"     => "",
               "title"           => "",
               "views"           => "",
               "duration"        => "",
               "display_picture" => "",
               "link"            => ""
              }
      meta
    end
        
    ## Finds if video is from youtube or vimeo
    def _detect_service(vurl)
      uri = URI(vurl)
      case uri.host
      when "www.youtube.com", "youtube.com" then "youtube"
      when "www.vimeo.com", "vimeo.com"     then "vimeo"
      else                                          nil
      end  
    end
    
    def _create_key(id)
      Settings.app_key + ":video:" + id
    end

    def _read(key)
      key = _create_key(key) unless key.start_with?(Settings.app_key + ":video:")
      video = REDIS.get(key)
      if video
        return ActiveSupport::JSON.decode(video)
      end
      return nil
    end

    def _read_all(key)
      key = _create_key(key)
      keys = REDIS.keys(key)
      bundles = []
      keys.each do |key|
        bundles.push _read(key)
      end
      bundles
    end

    def _write(key, video)
      key = _create_key(key) unless key.start_with?(Settings.app_key + ":video:")
      if REDIS.set(key, video.to_json)
        Rails.logger.info "[OK] Write video to redis"
      else
        Rails.logger.info "[FAIL] Write video to redis"
      end
    end
  end
end