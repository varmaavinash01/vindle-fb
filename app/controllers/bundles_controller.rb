class BundlesController < ApplicationController
  before_filter :current_user
  before_filter :login_check
  def index
    @bundles = Bundle.all
    @bundles  = @bundles.sort_by {|k,v| v}.reverse
=begin
    #post to yammer activity
    options = {
      "activity" => "avinash varma",
      "actor" => {"name" => "avinash"},
      "email" => "avinash.varma@mail.rakuten.com",
      "action" => "create",
      "object" => {
          "url" => "http://vindle.it:4000/auth/index",
          "title" => "Video library"
          },
      "message" => "activity test"
    }
    https://www.yammer.com/api/v1/activity.json?access_token=ABC123
    uri = URI.parse("https://www.yammer.com/api/v1/activity.json")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, options.to_json)
    req['access_token'] = ''
    res = https.request(req)
    Rails.logger.info "******* resp of activity feed" + res.body.to_json
=end
    
  end

  def new
  end

  def create
    bundle = Bundle.create(params)
    Bundle.save(bundle)
    redirect_to :action => "index"
  end

  def show
    @bundle = Bundle.find(params[:id])
    @bundle["created_by"] = User.get(@bundle["created_by"])
    @videos = []
    @bundle["videos"].each do | vid |
      video = Video.find(vid)
      if video
        @videos.push video
      else
        Bundle.removeVid(@bundle["bid"], vid)
        ##  need to delete zombie video from bundle
      end
    end
    @videos = @videos.reverse
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
